# README #

## Introduction:  

The on-boarding process during my internship at ** [The Real Estate Social Network](www.resaas.com)**  required me to ask a question to all employees and send out the results.
For this purpose, instead of sending out a plain text email, I created an application powered by AngularJs and hosted in to an Azure server.

Click on the following link to view the application: **[RESAAS - On Boarding](http://resaashappapp.azurewebsites.net/)**

Moving forward, I will turn this application into a more sophisticated common repository for answers to all on-boarding questions at  [The Real Estate Social Network](www.resaas.com)


## Technologies: ##

* HTML
* CSS
* AngularJs
* Firebase

## Purpose: ##

* Learn and apply AngularJs framework to create an application.
* Integrate the Firebase API to make the application run in realtime.

## Procedure: ##

1. Browse to **[RESAAS - On Boarding](http://resaashappapp.azurewebsites.net/)**



## Timestamp: ##

**January 2015 – March 2015