resaasApp.controller('mainController',  ['$scope', 'resaasService', function($scope, resaasService) {
    $scope.name="";
    $scope.employees = resaasService.employees;
    
    
}]);

resaasApp.controller('resultController', ['$scope', '$routeParams', 'resaasService', function($scope, $routeParams, resaasService) {
    
	var employee = resaasService.getEmployee($routeParams.id);
    var questions = resaasService.questions;
    
    
    //Assigning values to scope	
	$scope.name = employee.name;
	$scope.team = employee.team;
	$scope.age = employee.age;
	$scope.answer1 = employee.a1;
    $scope.answer2 = employee.a2;
    $scope.answer3 = employee.a3;
    $scope.answer4 = employee.a4;
    $scope.answer5 = employee.a5;
    
    $scope.question1 = questions[0].$value;
    $scope.question2 = questions[1].$value;
    $scope.question3 = questions[2].$value;
    $scope.question4 = questions[3].$value;
    $scope.question5 = questions[4].$value;
    
    
    //Editing user information
    $scope.editSubmit = function() {
        if ($scope.name, $scope.team, $scope.age, $scope.answer1, $scope.answer2, $scope.answer3, $scope.answer4, $scope.answer5 ) {
            
            resaasService.editEmployee($scope.name, $scope.team, $scope.age, $scope.answer1, $scope.answer2, $scope.answer3, $scope.answer4, $scope.answer5, $routeParams.id );
            
        }
        else {
            console.log("Please fill in all the required fields");
        }
    
    
    };
    
    

}]);

resaasApp.controller('aboutController', ['$scope', '$routeParams', 'resaasService', function($scope, $routeParams, resaasService) {
    
    $scope.length = resaasService.getLength() + 1;
    
    
    $scope.submit = function() {
        if ($scope.fullName, $scope.team, $scope.age, $scope.answer1, $scope.answer2, $scope.answer3, $scope.answer4, $scope.answer5 ) {
            resaasService.addEmployee($scope.fullName, $scope.team, $scope.age, $scope.answer1, $scope.answer2, $scope.answer3, $scope.answer4, $scope.answer5, $scope.length );
        }
        else {
            console.log("Please fill in all the required fields");
        }
      };
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}]);