resaasApp.service('resaasService', function($http, $firebase) {
    
    //Fetch employees from firebase
    var empref = new Firebase("https://blinding-fire-3871.firebaseio.com/employees");
    var empsync = $firebase(empref);
    var friends = empsync.$asArray();
    
    
    //fetch questions from firebase
    var questref = new Firebase("https://blinding-fire-3871.firebaseio.com/questions");
    var questsync = $firebase(questref);
    var questions = questsync.$asArray();
    
    //Assign Employees and questions to the service properties
    this.questions = questions;
    
    this.employees = friends;
    
    
    this.getEmployee = function(id) {
		
		for (var i=0,len=friends.length;i<len;i++) {
            
			
			if(friends[i].id == id) {
				return friends[i];
			}   
    	}	
	};
    
    this.getLength = function() {
        var length = friends.length;;
        return length;
	};
    
    
    this.addEmployee = function(empname, empteam, empage, empa1, empa2, empa3, empa4, empa5, empid) {
        friends.$add({ name: empname,
                      team: empteam,
                      age: empage,
                      a1: empa1,
                      a2: empa2,
                      a3: empa3,
                      a4: empa4,
                      a5: empa5,
                      id: empid
                     }).then(function(empref) {
              var id = empref.key();
             alert("added record with id " + id);
              friends.$indexFor(id); // returns location in the array
            });       
    };
	

	this.editEmployee = function (newname, newteam, newage, newa1, newa2, newa3, newa4, newa5, empId) {
        
        var id  = empId - 1;   
        console.log(id);
        friends[id].name = newname;
        friends[id].team = newteam;
        friends[id].age = newage;
        friends[id].a1 = newa1;
        friends[id].a2 = newa2;
        friends[id].a3 = newa3;
        friends[id].a4 = newa4;
        friends[id].a5 = newa5;
        friends[id].id = empId;

        friends.$save(id).then(function(empref) {
            alert('Done!');
            empref.key() === friends[id].$id; // true
        });
        
    };
	

    
    
});