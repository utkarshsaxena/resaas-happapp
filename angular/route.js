resaasApp.config(function ($routeProvider) {
    
    $routeProvider
    
    .when('/', {
        templateUrl:'pages/main.html',
        controller: 'mainController'
    })
    
    .when('/result/:id', {
        templateUrl:'pages/result.html',
        controller: 'resultController'
    })
    
    .when('/dashboard', {
        templateUrl:'pages/dashboard.html',
        controller: 'aboutController'
    })
    
    
});