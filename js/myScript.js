var resaasApp = angular.module('resaasApp', ['ngRoute', 'ngAnimate', 'firebase']);

function validate() {
    var username = document.getElementById("InputEmail").value;
    var password = document.getElementById("InputPassword").value;
    var ref = new Firebase("https://blinding-fire-3871.firebaseio.com");
        ref.authWithPassword({
          email    : username,
          password : password
        }, function(error, authData) {
          if (error) {
            console.log("Login Failed!", error);
          } else {
              alert("Authenticated successfully with payload:", authData);
            window.location = "index.html";
            
          }
        });
}

function unvalidate() {
    var ref = new Firebase("https://blinding-fire-3871.firebaseio.com");
    ref.unauth();
    window.location = "index.html";
}

var ref = new Firebase("https://blinding-fire-3871.firebaseio.com");
var authData = ref.getAuth();
if (authData) {
    jQuery('.logInForm').hide();
    jQuery('.logOut').show();
} else {
  jQuery('.logInForm').show();
    jQuery('.logOut').hide();
}